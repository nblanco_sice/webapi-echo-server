﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication8.Controllers
{
    
    public class EchoController : Controller
    {
        // GET: api/<controller>
        [Route("api/[controller]")]
        [HttpGet]
        public string Get()
        {
            var headers = Request.Headers;
            Console.WriteLine("GET request created at " + headers["timestamp"].ToString());
            return headers["timestamp"];
        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/[controller]/{entity_type?}/{entity_id?}")]
        public string Post(string entity_type,string entity_id,[FromBody] string content)
        {
            var headers = Request.Headers;
            Console.WriteLine("POST request created at "+headers["timestamp"].ToString());
            return "OK";
        }


        // POST api/<controller>
        [HttpPost]
        [Route("api/[controller]/{entity_type}")]
        public string Post(string entity_type, [FromBody] string content)
        {
            var headers = Request.Headers;
            Console.WriteLine("POST request created at " + headers["timestamp"].ToString());
            return "OK";
        }

        // PUT api/<controller>
        [HttpPut]
        [Route("api/[controller]/{entity_type?}/{entity_id?}")]
        public void Put([FromBody]string value)
        {
            
            var headers = Request.Headers;
            Console.WriteLine("PUT request created at " + headers["timestamp"].ToString());
        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/")]
        public string Post([FromBody] string content)
        {
            var headers = Request.Headers;
            Console.WriteLine("POST request created at " + headers["timestamp"].ToString());
            return "OK";
        }
    }
}
